USE [master]
GO
/****** Object:  Database [Grupo1_DW]    Script Date: 27/01/2020 20:52:38 ******/
CREATE DATABASE [Grupo1_DW]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Grupo1_DW', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MULTIDIMENSIONAL\MSSQL\DATA\Grupo1_DW.mdf' , SIZE = 139264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Grupo1_DW_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MULTIDIMENSIONAL\MSSQL\DATA\Grupo1_DW_log.ldf' , SIZE = 860160KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Grupo1_DW] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Grupo1_DW].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Grupo1_DW] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Grupo1_DW] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Grupo1_DW] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Grupo1_DW] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Grupo1_DW] SET ARITHABORT OFF 
GO
ALTER DATABASE [Grupo1_DW] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Grupo1_DW] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Grupo1_DW] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Grupo1_DW] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Grupo1_DW] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Grupo1_DW] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Grupo1_DW] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Grupo1_DW] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Grupo1_DW] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Grupo1_DW] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Grupo1_DW] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Grupo1_DW] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Grupo1_DW] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Grupo1_DW] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Grupo1_DW] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Grupo1_DW] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Grupo1_DW] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Grupo1_DW] SET RECOVERY FULL 
GO
ALTER DATABASE [Grupo1_DW] SET  MULTI_USER 
GO
ALTER DATABASE [Grupo1_DW] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Grupo1_DW] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Grupo1_DW] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Grupo1_DW] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Grupo1_DW] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Grupo1_DW', N'ON'
GO
ALTER DATABASE [Grupo1_DW] SET QUERY_STORE = OFF
GO
USE [Grupo1_DW]
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetLastDayOfMonth]    Script Date: 27/01/2020 20:52:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Listing 3
------------

CREATE FUNCTION [dbo].[fnGetLastDayOfMonth]
-- Input parameters
(
@Anydate datetime
)
RETURNS datetime AS
/********************************************************************
Returns the last day of the month (extracted from the date passed)
*********************************************************************/
BEGIN
-- add one month to the datepassed
SET @Anydate = DATEADD(m,1,@Anydate)
RETURN DATEADD(d,-datepart(d,@Anydate),@Anydate)
END

GO
/****** Object:  Table [dbo].[Dim_Client]    Script Date: 27/01/2020 20:52:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dim_Client](
	[ID_Client] [int] NOT NULL,
	[PersonType] [nchar](15) NULL,
	[NameStyle] [int] NULL,
	[Title] [nvarchar](8) NULL,
	[FirstName] [nvarchar](50) NULL,
	[MiddleName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Suffix] [nvarchar](10) NULL,
	[EmailPromotion] [int] NULL,
	[AdditionalContactInfo] [nvarchar](max) NULL,
	[DemographicsPerson] [nvarchar](max) NULL,
	[AccountNumber] [nvarchar](15) NULL,
	[NameSalesTerritory] [nvarchar](50) NULL,
	[CountryRegionCode] [nvarchar](3) NULL,
	[Group] [nvarchar](50) NULL,
	[SalesYTD] [money] NULL,
	[SalesLastYear] [money] NULL,
	[CostYTD] [money] NULL,
	[CostLastYear] [money] NULL,
	[NameStore] [nvarchar](50) NULL,
	[DemographicsStore] [nvarchar](max) NULL,
 CONSTRAINT [PK_Dim_Client] PRIMARY KEY CLUSTERED 
(
	[ID_Client] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dim_Payment]    Script Date: 27/01/2020 20:52:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dim_Payment](
	[ID_Payment] [int] NOT NULL,
	[PaymentType] [nvarchar](50) NULL,
 CONSTRAINT [PK_Dim_Pay] PRIMARY KEY CLUSTERED 
(
	[ID_Payment] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dim_Product]    Script Date: 27/01/2020 20:52:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dim_Product](
	[ID_Product] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[ProductNumber] [nvarchar](25) NULL,
	[MakeFlag] [bit] NULL,
	[FinishedGoodsFlag] [bit] NULL,
	[Color] [nvarchar](15) NULL,
	[SafetyStockLevel] [smallint] NULL,
	[ReorderPoint] [smallint] NULL,
	[StandardCost] [money] NULL,
	[ListPrice] [money] NULL,
	[Size] [nvarchar](5) NULL,
	[SizeUnitMeasureCode] [nchar](3) NULL,
	[WeightUnitMeasureCode] [nchar](3) NULL,
	[Weight] [decimal](8, 2) NULL,
	[DaysToManufacture] [int] NULL,
	[ProductLine] [nchar](2) NULL,
	[Class] [nchar](2) NULL,
	[Style] [nchar](2) NULL,
	[ProductSubcategoryID] [int] NULL,
	[ProductModelID] [int] NULL,
	[SellStartDate] [datetime] NULL,
	[SellEndDate] [datetime] NULL,
	[DiscontinuedDate] [datetime] NULL,
 CONSTRAINT [PK_Dim_Product] PRIMARY KEY CLUSTERED 
(
	[ID_Product] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dim_SpecialOffer]    Script Date: 27/01/2020 20:52:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dim_SpecialOffer](
	[ID_SpecialOffer] [int] NOT NULL,
	[SpecialOfferID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[Type] [nvarchar](50) NULL,
	[Category] [nvarchar](50) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[MinQty] [int] NULL,
	[MaxQty] [int] NULL,
 CONSTRAINT [PK_Dim_SpecialOffer] PRIMARY KEY CLUSTERED 
(
	[ID_SpecialOffer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DIM_TIME]    Script Date: 27/01/2020 20:52:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DIM_TIME](
	[ID] [int] NOT NULL,
	[OrderDate] [datetime] NULL,
	[DATEKEY] [nvarchar](8) NULL,
	[DD] [nvarchar](2) NULL,
	[MM] [nvarchar](2) NULL,
	[YY] [varchar](4) NULL,
 CONSTRAINT [PK_DIM_TIME] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[H_Offers]    Script Date: 27/01/2020 20:52:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[H_Offers](
	[id_Product] [int] NOT NULL,
	[id_SpecialOffer] [int] NOT NULL,
	[DateKey] [int] NOT NULL,
	[id_Client] [int] NOT NULL,
	[id_Payment] [int] NOT NULL,
	[DiscountPct] [smallmoney] NULL,
	[OrderQty] [smallint] NULL,
	[UnitPrice] [money] NULL,
	[UnitPriceDiscount] [money] NULL,
	[LineTotal] [money] NULL,
	[TotalDue] [money] NULL,
 CONSTRAINT [PK_H_Offerts] PRIMARY KEY CLUSTERED 
(
	[id_Product] ASC,
	[id_SpecialOffer] ASC,
	[DateKey] ASC,
	[id_Client] ASC,
	[id_Payment] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[H_Offers]  WITH CHECK ADD  CONSTRAINT [FK_H_Offers_Dim_Client] FOREIGN KEY([id_Client])
REFERENCES [dbo].[Dim_Client] ([ID_Client])
GO
ALTER TABLE [dbo].[H_Offers] CHECK CONSTRAINT [FK_H_Offers_Dim_Client]
GO
ALTER TABLE [dbo].[H_Offers]  WITH CHECK ADD  CONSTRAINT [FK_H_Offers_Dim_Payment] FOREIGN KEY([id_Payment])
REFERENCES [dbo].[Dim_Payment] ([ID_Payment])
GO
ALTER TABLE [dbo].[H_Offers] CHECK CONSTRAINT [FK_H_Offers_Dim_Payment]
GO
ALTER TABLE [dbo].[H_Offers]  WITH CHECK ADD  CONSTRAINT [FK_H_Offers_Dim_Product] FOREIGN KEY([id_Product])
REFERENCES [dbo].[Dim_Product] ([ID_Product])
GO
ALTER TABLE [dbo].[H_Offers] CHECK CONSTRAINT [FK_H_Offers_Dim_Product]
GO
ALTER TABLE [dbo].[H_Offers]  WITH CHECK ADD  CONSTRAINT [FK_H_Offers_Dim_SpecialOffer] FOREIGN KEY([id_SpecialOffer])
REFERENCES [dbo].[Dim_SpecialOffer] ([ID_SpecialOffer])
GO
ALTER TABLE [dbo].[H_Offers] CHECK CONSTRAINT [FK_H_Offers_Dim_SpecialOffer]
GO
ALTER TABLE [dbo].[H_Offers]  WITH CHECK ADD  CONSTRAINT [FK_H_Offers_DIM_TIME] FOREIGN KEY([DateKey])
REFERENCES [dbo].[DIM_TIME] ([ID])
GO
ALTER TABLE [dbo].[H_Offers] CHECK CONSTRAINT [FK_H_Offers_DIM_TIME]
GO
USE [master]
GO
ALTER DATABASE [Grupo1_DW] SET  READ_WRITE 
GO
